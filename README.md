# lynnara-kit
lynnara-kit

[![pipeline status](https://gitlab.com/nihililix/lynnara-kit/badges/master/pipeline.svg)](https://gitlab.com/nihililix/lynnara-kit/-/commits/master)

## Contents
1. Quickstart
2. Install
3. Frontend
4. Kirby (localhost)
5. Features / Dependencies

## Quickstart

- Install `git clone --recursive https://gitlab.com/nihililix/lynnara-kit.git` the repository
- install dependencies `yarn`
- build frontend `yarn build`
- or serve with `yarn serve` and `php -S localhost:8000 kirby/router.php` 

## Install

##### Installation 
Install default files and kirby as git submodules
```
git clone --recursive https://gitlab.com/nihililix/lynnara-kit.git
```

or 
```
git submodule init 
git submodule update
```

## Frontend

##### Installation
```
yarn
```

##### Build Prod Version
```
yarn build
```

##### Build Dev Version
```
yarn serve
```
## Kirby

##### Running from Command Line 
go in project folder 'www'
```
php -S localhost:8000 kirby/router.php
```
http://localhost:8000

## Documentations
# Kirby git
https://github.com/OblikStudio/kirby-git

## Features / Dependencies
* **Kirby (CMS)**
* scss
* es6
* webpack
* stylelint
* eslint
* browserlist
