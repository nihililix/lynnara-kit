<?php

header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400'); // cache for 1 day

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("HTTP/1.1 200 OK");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        header("Access-Control-Allow-Headers: Content-type, Authorization, Origin, X-Requested-With'");
}

return [
    'panel' =>[
        'install' => true
    ],
    'oblik.git.repo' => 'content',
    'oblik.git.merge' => 'live',
    'debug' => true,
    'languages' => true,
    'api' => [
        'basicAuth' => true,
        'allowInsecure' => false # DO NOT USE THIS IN PRODUCTION!
    ],
    'thumbs' => [
        'presets' => [
            'test' => ['width' => 100, 'quality' => 80],
        ]
    ]
];
