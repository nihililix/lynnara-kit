/**
 * Webpack Config for production environment
 */
const Webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.config.common.js');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = merge(common, {
    mode: 'production',
    devtool: 'source-map',
    stats: 'errors-warnings',
    bail: true,
    output: {
        // filename: 'js/[name].[chunkhash:8].js',
        filename: 'js/[name].js',
        chunkFilename: 'js/[name].[chunkhash:8].chunk.js'
    },
    plugins: [
        // Clean Output path before new build
        new CleanWebpackPlugin(),
        new Webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new MiniCssExtractPlugin({
            // filename: '[hash].bundle.css' // TODO: add hash
            filename: 'bundle.css'
        }),
        new ManifestPlugin(),
        new Webpack.optimize.ModuleConcatenationPlugin()
    ],
    module: {
        rules: []
    }
});
